%{
#include <stdio.h>
#include <math.h>
#include "sql_header_functions.h"
void yyerror(const char *);
extern int yylex(void); 
extern int yylineno;
extern FILE *yyin;
%}

%error-verbose /* gives more descriptive errors */
%token SELECT DISTINCT FROM AS CONC INT NAME QUOTED SQUOTED WHERE BETWEEN AND IN LIKE IS NOT OR NULLx
%left '+' '-'
%left '*' '/'
%%

prog	:	
		| prog line
;

line	: ';'
		| SELECT col FROM NAME ';'				{printLine("SELECT");}
		| SELECT DISTINCT col FROM NAME ';' 	{printLine("DISTINCT");}
		| SELECT col FROM NAME WHERE cond ';'
		| error ';'								{yyerrok;}
;

cond 	: exp
		| exp OR exp					{printLine("OR");}
		| exp AND exp					{printLine("AND");}
		| col NOT IN col				{printLine("NOT IN");}
		| col NOT BETWEEN INT AND INT	{printLine("NOT BETWEEN");}
		| col NOT LIKE SQUOTED			{printLine("NOT LIKE");}
		| col IS NOT NULLx				{printLine("IS NOT NULL");}
		| col BETWEEN INT AND INT 		{printLine("BETWEEN");}
		| col IN col 					{printLine("IN");}
		| col LIKE SQUOTED 				{printLine("LIKE");}
		| col IS NULLx 					{printLine("NULL");}
		| col OR col					{printLine("OR");}
;

exp		:
		| col '=' col
		| col '>'col
		| col '<'col
		| col '>''='col
		| col '<''='col
		| col '<''>'col
;

col		: '*'
		| NAME
		| INT
		| col ',' col 
		| col '*' col
		| col '+' col
		| col '-' col
		| col '/' col
		| QUOTED
		| SQUOTED
		| col CONC col
		| col col
		| col AS col
		| '(' col ')'

;

%%
void yyerror (const char *s){
	printf ("error %s\n", s);
}

int main (int argc, char **argv){
	if (argc <2)
		return -1;
	yyin = fopen(argv[1], "r");
	if(yyin)
		yyparse();
	if (argv[2] > 0) {	
	yyin = fopen(argv[2], "r");
	if(yyin)
		yyparse();
	}
	return 0;	
}